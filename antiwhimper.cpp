#include <alsa/asoundlib.h>
#include <csignal>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <vector>


static void alsa_call_failed(const char* function_name, int err)
{
	std::stringstream buf;
	buf << function_name;
	buf << "() failed: ";
	buf << snd_strerror(err);
	throw std::runtime_error(buf.str());
}


static void alsa_call(const char* function_name, int err)
{
	if (err < 0)
	{
		alsa_call_failed(function_name, err);
	}
}


#define ALSA_CALL(function_name, ...) alsa_call(#function_name, function_name(__VA_ARGS__))


/**
 * Simple wrapper around ALSA PCM API to provide RAII
 * (Resource Aquisition is Initialization)
 */
struct PCM
{
	snd_pcm_t* handle;
	PCM(const char* dev)
		: handle(0)
	{
		ALSA_CALL(snd_pcm_open, &handle, dev, SND_PCM_STREAM_PLAYBACK, 0);
		// set hw params
		ALSA_CALL(snd_pcm_set_params,
				handle,
				SND_PCM_FORMAT_S16, // format
				SND_PCM_ACCESS_RW_INTERLEAVED, // access
				2, // channels,
				44100, // rate
				0, // soft_resample
				10000000); // latency = 1s
	}
	~PCM()
	{
		if (handle)
		{
			snd_pcm_drain(handle);
			snd_pcm_close(handle);
		}
	}
	void write(std::vector<unsigned char>& buf)
	{
		ALSA_CALL(snd_pcm_writei, handle, buf.data(), buf.size());
	}
};


bool keep_running = true;


struct SignalHandler : public sigaction
{
	int signum;
	sigaction oldact;
	SignalHandler(int signum)
		: signum(signum)
	{
		memset(static_cast<sigaction*>(this), 0, sizeof(sigaction));
		sa_handler = SignalHandler::handler;
		::sigaction(signum, this, &oldact);
	}
	~SignalHandler()
	{
		::sigaction(signum, &oldact, 0);
	}
	static void handler(int signum)
	{
		(void) signum; // unused
		keep_running = false;
	}
};


int main(int argc, char** argv)
{
	const char* dev;
	switch (argc) {
		case 1:
   			dev = "hw:0,0";
			break;
		case 2:
			dev = argv[1];
			break;
		default:
			std::cerr << "Usage: " << argv[0] << "[alsa-device-string]" << std::endl;
			std::cerr << "alsa-device-string defaults to 'hw:0,0'" << std::endl;
			return EXIT_FAILURE;
	}
	try
	{
		SignalHandler signalHandler(SIGINT);
		PCM pcm(dev);
		// 16KiB of zeroes
		std::vector<unsigned char> buf(16*1024);
		for (int i=0; i<16*1024; ++i)
		{
			buf.push_back(0);
		}
		while (keep_running)
		{
			pcm.write(buf);
		}
	}
	catch (std::runtime_error& e)
	{
		std::cerr << "Exception caught: " << e.what() << std::endl;
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}
