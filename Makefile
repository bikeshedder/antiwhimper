ASOUND_CFLAGS := $(shell pkg-config --cflags alsa)
ASOUND_LFLAGS := $(shell pkg-config --libs alsa)

CXXFLAGS=-std=c++11 -pedantic -Wall -Wextra $(IDL_CFLAGS) $(ASOUND_CFLAGS) -g
LFLAGS=$(ASOUND_LFLAGS)

OBJS=antiwhimper.o
BIN=antiwhimper

all: $(BIN)

$(BIN): $(OBJS)
	$(CXX) -o $@ $< $(LFLAGS)

clean:
	rm -f $(OBJS) $(BIN)
